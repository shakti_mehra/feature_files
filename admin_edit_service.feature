Feature: Admin Services Crud

Background: 
  Given I am logged in as Admin
  Then I click on services


Scenario: Creating new service successfully
  When I click on new service button
  And I enter name and select a duration on new service page
  And I click on create service
  Then a new service should be created

Scenario: Creating new service failed due to blank name
  When I click on new service button
  And I leave name field blank on new service page
  And I click on create service
  Then I get an error saying name can't be blank

Scenario: Creating new service failed due to duplicate name
  When I click on new service button
  And I lfill in a duplicate name on new service page
  And I click on create service
  Then I get an error saying name has already been taken

Scenario: Viewing a service
  Given I created a service previously
  When I click on show button corresponding to the service
  Then I get redirected to the view service page
  And I click on edit
  Then the edit service page should open

Scenario:  Editing a service
  When I click on edit button corresponding to the service
  Then edit service page should open
  And I modify the name or duration or status and click on update service
  Then the settings should get saved