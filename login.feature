Feature: User Log in

  Background: I am on User login page
    Given I am on user Login page
            

    Scenario: Logged in successfully
          When I login with valid credentials
          Then I get logged in
          And I should be on Homepage


    Scenario Outline: Login failed
          When I login with wrong credential <email> , <password>
          Then I should see error message

    Examples:           |    email       |  Password  |
                     |               |            |
               | shakti@vinsol.com   |            |
                |                    |  12345678  |
               | shakti@abcd.com     |  12345678  |
                |shakti@vinsol.com   |  11111111  |


