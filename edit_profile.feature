Feature: Edit Profile


Background:
  Give I am logged in and on homepage
  Then I click on edit profile


  Scenario: Name and password edited successfully
    When I modify my name and enter a new password and confirm the same
    And I enter the corect old password and click on update
    Then I should get ridirected to home page and my updated info should get saved


  Scenario: Editing failed due to invalid old password
    When I enter incorrect old password and click on update
    Then I should get an error saying incorrect current password


  Scenario Outline:  Editing failed due to invalid new password
    When I enter an invalid <new password> and valid old password and click on update
    Then I should get an error saying invalid new password

  Examples:         |new password|
                    |  "     "   |
                    |  "abcd"    |
                    |  "1234"    |
                    |  "1       "|

