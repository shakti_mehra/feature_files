Feature: User Sign up

  Background: 
    Given I am on user sign up page


  Scenario: User sign up successful
    When I enter name and valid email and password and click on sign up
    Then I should receive a confirmation email
    And clicking on confirmation link should redirect me to the logged in homepage


  Scenario: Sign up unsuccessful due to invalid password
    When I an invalid or blank password and click on Sign up
    Then I should get an error saying invalid password