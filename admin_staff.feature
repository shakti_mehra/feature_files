Feature:  Admin Staff Crud

Background:
  Given I am logged in as Admin
  Then I click on staff

Scenario: Creating new staff successfully
  When I click on new staff button
  And I enter name, designation, email and select services on new staff page
  And I click on create service
  Then a new service should be created

Scenario: Creating new service failed due to blank field
  When I click on new service button
  And I leave any field blank on new service page
  And I click on create service
  Then I get an error saying that it can't be blank


Scenario: Creating new service failed due to duplicate field
  When I click on new service button
  And I fill in duplicate email or name on new service page
  And I click on create service
  Then I get an error saying that name/email has already been taken


Scenario: Viewing a staff
  Given I created a staff previously
  When I click on show button corresponding to the staff
  Then I get redirected to the view staff page
  And I click on edit
  Then the edit staff page should open

Scenario:  Editing a staff
  When I click on edit button corresponding to the staff
  Then edit staff page should open
  And I modify the name or desgination or services and click on update staff
  Then the settings should get saved
